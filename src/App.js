import React from 'react';
import logo from './logo.svg';
import './App.css';
import { FormattedMessage, FormattedHTMLMessage} from "react-intl";


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">
            <FormattedMessage id="app.title"
                              defaultMessage="Welcome to Default"
                              description="Welcome header on app main page"/>
        </h1>


        <table className="egt">
          <tr>
            <td>
              <label>
                <FormattedMessage id="login.section.label.email" defaultMessage="..." description="Welcome header on app main page"/>
              </label>
            </td>
            <td>
              <input type="text" name="email"/>
            </td>
          </tr>
          <tr>
            <td>
              <label>
                <FormattedMessage id="login.section.label.password" defaultMessage="..." description="Welcome header on app main page"/>
              </label>
            </td>
            <td>
              <input type="password" name="password"/>
            </td>
          </tr>
          <tr>
            <td>
              <button>
                <FormattedMessage id="login.section.button.continue" defaultMessage="..." description="Welcome header on app main page"/>
              </button>
            </td>
          </tr>
          <tr>
            <td>
              <button>
                <FormattedMessage id="login.section.button.login" defaultMessage="..." description="Welcome header on app main page"/>
              </button>
            </td>
          </tr>
        </table>




        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Andy Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
